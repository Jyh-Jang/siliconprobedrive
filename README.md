# Silicon Probe Drive #

Here documents necessary materials and steps to get your silicon probe drive up and running.
![isometricview](/images/isometricview_crop.PNG)
### What is this repository for? ###

* This drive is invented in [Kloosterman lab](https://www.nerf.be/research/nerf-labs/fabian-kloosterman) at [NERF](http://www.nerf.be).
* Version 1.0

### What do I need? ###

* Print out the 4 STL files. We have tested grey resin and printed by Form 2 & 3 (Formlabs). 
* A custom screw (14.663mm-long M1.2; resolution: 250 µm per turn). The one from [OpenEphys](https://open-ephys.org/drive-implant/drive-screwdriver-6se4e) might work.
* 2x 20G metal tubings (2mm-long) and 2X 23G metal tubings (8mm-long). Both could be ordered from [Micro group](https://www.microgroup.com/).
* 2x 6mm-long 1.2M screw; 4x 3mm-long 1.2M screw.
* 4x [female-male breakaway header pins](https://www.amazon.com/Single-Headers-Machine-Female-2-54mm/dp/B0187LHLDC/ref=sr_1_7?dchild=1&keywords=2.54mm+break-away+pins&qid=1595152926&sr=8-7).
* A screwdriver. The one from [OpenEphys](https://open-ephys.org/drive-implant/drive-screwdriver) might work.
* A silicon probe for chroinc recording. I have tested and assembled chronic probes from [ATLAS Neuroengineering](http://www.atlasneuro.com/en/products/passive-probes) and [Cambridge NeuroTech](https://open-ephys.org/drive-implant/drive-screwdriver).

### What are the design goals/strategies of this drive? ###
* Lightweight: 3g. Suitable for mice.
* Assembled in a day.
* Travel distance: 7 mm
* Reuseablility: The body part including the probe can be withdwawed by inserting a at least 10mm-long M1.8 screw. 

### Where could I download the STL files? ###
* /STL/

### Where could I find protocol for assembling? ###
* /protocol/ (Documented by [Hanna Den Bakker](https://www.nerf.be/people/hanna-den-bakker-1) )

### Who do I talk to? ###
* Repo owner: [Jyh-Jang Sun](https://www.nerf.be/people/jyh-jang-sun), e-mail: Jyh-Jang.Sun@nerf.be
* First upload: July 18 2020; Last update: July 24 2020

### More photos ###
* Bird view
![birdview](/images/birdview.PNG)
* Side view
![sideview](/images/sideview.PNG)
* Top view
![topview](/images/topview.PNG)
* Front view
![frontview](/images/frontview.PNG)

### License ###
Copyright Jyh-Jang Sun, Hanna Den Bakker, Fabian Kloosterman 2020.

This documentation describes Open Hardware and is licensed under the CERN OHL-S v.2.

You may redistribute and modify this documentation under the terms of the [CERN OHL-S v.2](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2). This documentation is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN OHL-S v.2 for applicable conditions